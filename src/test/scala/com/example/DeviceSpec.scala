package com.example

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import scala.concurrent.duration._

class DeviceSpec
    extends TestKit(ActorSystem("device-system"))
    with WordSpecLike
    with Matchers
    with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "The device actor" must {
    "reply with empty reading if no temperature is known" in {
      val testProbe = TestProbe()
      val deviceActor = system.actorOf(Device.props("aGroup", "aDevice"))

      deviceActor.tell(Device.ReadTemperature(requestId = 1), testProbe.ref)
      val response = testProbe.expectMsgType[Device.RespondTemperature]
      response.requestId shouldEqual 1
      response.value shouldEqual None
    }

    "reply with latest reading" in {
      val probe = TestProbe()
      val deviceActor = system.actorOf(Device.props("aGroup", "aDevice"))

      deviceActor.tell(Device.RecordTemperature(requestId = 1, value = 25.0), probe.ref)
      probe.expectMsg(Device.TemperatureRecorded(requestId = 1))

      deviceActor.tell(Device.ReadTemperature(requestId = 2), probe.ref)
      val response = probe.expectMsgType[Device.RespondTemperature]
      response.requestId shouldEqual 2
      response.value shouldEqual Some(25.0)
    }

    "reply to registration requests" in {
      val probe = TestProbe()
      val deviceActor = system.actorOf(Device.props("aGroup", "aDevice"))

      deviceActor.tell(DeviceManager.RequestTrackDevice("aGroup", "aDevice"), probe.ref)
      probe.expectMsg(DeviceManager.DeviceRegistered)
      probe.lastSender shouldEqual deviceActor
    }

    "ignore wrong registration requests" in {
      val probe = TestProbe()
      val deviceActor = system.actorOf(Device.props("aGroup", "aDevice"))

      deviceActor.tell(DeviceManager.RequestTrackDevice("wrongGroup", "aDevice"), probe.ref)
      probe.expectNoMessage(500.millisecond)

      deviceActor.tell(DeviceManager.RequestTrackDevice("aGroup", "wrongDevice"), probe.ref)
      probe.expectNoMessage(500.millisecond)
    }
  }
}
