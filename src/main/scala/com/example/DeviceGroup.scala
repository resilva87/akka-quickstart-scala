package com.example

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

class DeviceGroup(groupId: String) extends Actor with ActorLogging {

  var deviceIdToActor = Map.empty[String, ActorRef]

  override def preStart(): Unit = log.info(s"DeviceGroup ${groupId} started")
  override def postStop(): Unit = log.info(s"DeviceGroup ${groupId} stopped")

  override def receive: Receive = {
    case trackMsg @ DeviceManager.RequestTrackDevice(`groupId`, _) =>
      deviceIdToActor.get(trackMsg.deviceId) match {
        case Some(deviceActor) =>
          deviceActor forward trackMsg
        case None =>
          log.info(s"Creating device actor for ${trackMsg.deviceId}")
          val deviceActor = context.actorOf(
            Device.props(groupId, trackMsg.deviceId),
            s"device-${trackMsg.deviceId}")

          context.watch(deviceActor)
          
          deviceIdToActor += (trackMsg.deviceId -> deviceActor)
          deviceActor forward trackMsg
      }

    case DeviceManager.RequestTrackDevice(groupId, _) =>
      log.warning(
        s"Ignoring request to track group ${groupId}. Actor is responsible for ${this.groupId}")
  }

}

object DeviceGroup {
  def props(groupId: String): Props = Props(new DeviceGroup(groupId))
}
