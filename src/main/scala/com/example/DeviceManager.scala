package com.example

class DeviceManager {

}

object DeviceManager {

  final case class RequestTrackDevice(groupId: String, deviceId: String)
  final case object DeviceRegistered
}