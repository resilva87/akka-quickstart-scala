package com.example

import akka.actor.{Actor, ActorLogging, Props}

class Device(groupId: String, deviceId: String)
    extends Actor
    with ActorLogging {
  import Device._

  var lastValue: Option[Double] = None

  override def preStart(): Unit =
    log.info(s"Device ${groupId}-${deviceId} started")
  override def postStop(): Unit =
    log.info(s"Device ${groupId}-${deviceId} stopped")

  override def receive: Receive = {
    case ReadTemperature(requestId) =>
      sender() ! RespondTemperature(requestId, lastValue)

    case RecordTemperature(requestId, value) =>
      log.info(s"Recording new reading from ${requestId} with value ${value}")
      lastValue = Some(value)
      sender() ! TemperatureRecorded(requestId)

    case DeviceManager.RequestTrackDevice(`groupId`, `deviceId`) =>
      sender() ! DeviceManager.DeviceRegistered

    case DeviceManager.RequestTrackDevice(groupId, deviceId) =>
      log.warning(
        s"Ignoring request to track device ${groupId}-${deviceId}. Actor is responsible for tracking ${this.groupId}-${this.deviceId}")
  }

}

object Device {

  def props(groupId: String, deviceId: String): Props =
    Props(new Device(groupId, deviceId))

  final case class ReadTemperature(requestId: Long)
  final case class RespondTemperature(requestId: Long, value: Option[Double])
  final case class RecordTemperature(requestId: Long, value: Double)
  final case class TemperatureRecorded(requestId: Long)
}
